//
//  GameScene.swift
//  SushiTower
//
//  Created by Parrot on 2019-02-14.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    let cat = SKSpriteNode(imageNamed: "character1")
    let sushiBase = SKSpriteNode(imageNamed:"roll")
    
    var tower:[SKSpriteNode]  = []
    
    
    
    
    override func didMove(to view: SKView) {
        // add background
        let background = SKSpriteNode(imageNamed: "background")
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        // add cat
        cat.position = CGPoint(x:self.size.width*0.25, y:100)
        addChild(cat)
        
        // add base sushi pieces
        sushiBase.position = CGPoint(x:self.size.width*0.5, y: 100)
        addChild(sushiBase)
        
        for i in 0...5 {
            spawnSushi()
        }
        [ 200, 300, 400, 500, 600 ]
        
        
        
    }
    
    var stickPosition:String = ""
    
    func spawnSushi() {
        
        // 1. check how many pieces are in teh tower
        let numPieces = self.tower.count
        
        // if 0 pieces, put piece at the starting position
        let specialSushi = SKSpriteNode(imageNamed:"roll")
        
        if (numPieces  == 0) {
            // add sushi at starting position
            specialSushi.position = CGPoint(x:self.size.width*0.5, y: 200)
        }
        else if (numPieces > 0) {
            // 1. get the previous piece
            let previousPiece = self.tower[self.tower.count-1]
            
            // 2. add the new pieces at 50 pixel above it
            specialSushi.position = CGPoint(
                x:self.size.width*0.5,
                y:previousPiece.position.y + 100)
        }
        
        
        // randomly generate where the chopstick
        let stickDirection = Int.random(in: 1...3)
        
        if (stickDirection == 1) {
            // no sticks
        }
        if (stickDirection == 2) {
            // stick on left
            let leftStick = SKSpriteNode(imageNamed:"chopstick")
            leftStick.position.x = -80
            
            specialSushi.addChild(leftStick)
            
            if (self.tower.count == 0) {
                stickPosition = "left"
            }
        }
        else if (stickDirection == 3) {
            // stick on right
            let rightStick = SKSpriteNode(imageNamed: "chopstick")
            rightStick.position.x = 80;
            rightStick.xScale = -1
            
            specialSushi.addChild(rightStick)
            
            if (self.tower.count == 0) {
                stickPosition = "right"
            }
        }
        
        // add sushi to the screen
        addChild(specialSushi)
        
        // add the sushi to the tower
        tower.append(specialSushi)
        
    }
    
    
    
    
    
    override func update(_ currentTime: TimeInterval) {
        //specialSushi.position.y = specialSushi.position.y - 10
        //chopstick.position.y = chopstick.position.y - 10
        
        
    }
    
    var catPosition:String = "left"
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // This is the shortcut way of saying:
        //      let mousePosition = touches.first?.location
        //      if (mousePosition == nil) { return }
        guard let mousePosition = touches.first?.location(in: self) else {
            return
        }
        
        print(mousePosition)
        
        // IS STICK AND CAT IN SAME POSITION?
        // --- IF YES = DIE
        
        // 1. get the position of the stick  (left / right)
        // 2. get the position of the cat   (left / right)
        // 3. compare the cat position and stick postion    (left == left, right == right)
        print("Cat Position: \(self.catPosition)")
        print("Stick Position: \(self.stickPosition)")
        if (stickPosition == catPosition) {
            
            print("PLAYER DIES!!!")
        }
        
        
        
        
        
        
        // -- IF NO == KEEP GOING (HIT THE TOWER, REMOVE SUSHI, AND KEEP GOING)
        
        
        
        
        // HIT THE TOWER
        // ---------------------------
        
        // 1. animate the cat hitting the tower
        var punchTextures:[SKTexture] = []
        for i in 1...3 {
            let fileName = "character\(i)"
            //print("Adding: \(fileName) to array")
            punchTextures.append(SKTexture(imageNamed: fileName))
        }
        punchTextures.append(SKTexture(imageNamed:"character1"))
        
        // 2. Tell Spritekit to use that array to create your animation
        let punchingAnimation = SKAction.animate(
            with: punchTextures,
            timePerFrame: 0.1)
        
        // 3. Repeat the animation once
        self.cat.run(punchingAnimation)
        
        //-------------------------
        // 2. remove the sushi from the tower
        // -----------------------
        
        // 1. GET THE SUSHI FROM THE TOWER
        if (self.tower.count > 0) {
            let currentSushi = self.tower[0]  // self.tower.first
            
            if (currentSushi != nil) {
                // 2. REMOVE THE SUSHI FROM THE SCREEN
                currentSushi.removeFromParent()
                
                // 3. REMOVE THE SUSHI FROM THE TOWER
                self.tower.remove(at: 0)
                
                //print("Number of pieces left in tower: \(self.tower.count)")
            }
        }
        
        // -----------------
        // 3. DROP THE TOWER
        // -----------------
        
        // move all pieces down
        for (index, piece) in self.tower.enumerated() {
            //print("Piece \(index) y-position: \(piece.position.y)")
            piece.position.y = piece.position.y - 100
            
        }
        
        
        
        // GREAT LETS TRY IT!
        
        
        
        
        
        let CENTER = self.size.width / 2
        if (mousePosition.x < CENTER) {
            // left
            // - move cat to x = 25% of width
            cat.position = CGPoint(x:self.size.width*0.25, y:100)
            
            let leftHandUpAction = SKAction.scaleX(to: 1, duration: 0)
            self.cat.run(leftHandUpAction)
            
            self.catPosition = "left"
        }
        else {
            // right
            // - move cat to x = 80% of width
            cat.position = CGPoint(x:self.size.width*0.80, y:100)
            let rightHandUpAction = SKAction.scaleX(to: -1, duration: 0)
            self.cat.run(rightHandUpAction)
            
            
            self.catPosition = "right"
            
        }
        
        
    }
    
}

